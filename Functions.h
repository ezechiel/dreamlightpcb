/**
 * Exposure UV DREAM LIGHT PCB
 * 
 * Descr : Lib function for dream light pcb
 * Author : Ezechiel
 * Date : 2019-11-11
 * 
 * Compatible Arduino Nano V3
 */

void initLCD();
void initButtonAndProgram();
void scrollRightToLeft (byte line, String str, char ending);
void scrollLeftToRight (int line, String str, byte space);
void twinkleDisplay(unsigned int timing, byte loop);
void menu();
void exposeUV();
void calculExposeUV();
void successUV();
void initLoading();
void switchLoadingBank(byte bank);
void displayLoadingBar(byte percent);
void lcdClear();
void buttonBack();
void setTimer();