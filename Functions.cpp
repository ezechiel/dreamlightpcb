#include "Arduino.h"
#include "EEPROM.h"
#include <LiquidCrystal.h>
#include "Functions.h"
#include "Melody.h"

//Led
#define LEDWAKING A0
#define LEDACTIVE A1
#define LED_HIGH 0
#define LED_LOW 1

//UV
#define UVHOOD A2
#define UVDRAWER A3

//LCD config
#define RS 7
#define EN 8
#define D4 9
#define D5 10
#define D6 11
#define D7 12

#define ROW 2
#define COLS 16

#define PIN_LCD_CONTRAST 5
#define DEFAULT_LCD_CONTRAST 20

#define PIN_LCD_BACKLIGHT 6
#define DEFAULT_LCD_BACKLIGHT 255

//button
#define UP 13
#define DOWN 4
#define BACK 2
#define ENTER 3

#define BUTTON_HIGH 0
#define BUTTON_LOW 1

//Timer Exposure
#define DEFAUT_INCR_EXPOSURE 10

//Buzzer
#define PIN_BUZZER A5

LiquidCrystal lcd(RS, EN, D4, D5, D6, D7);

const char BLANK[] = "                ";
unsigned long debounceTimeStamp;
enum valueEeprom {FUSE_TIMER, TIMER, FUSE_MUSIC, SETMUSIC, FUSE_LCD_CONTRAST, SETLCD_CONTRAST, FUSE_LCD_BACKLIGHT, SETLCD_BACKLIGHT};
enum valueMenu {HOOD, DRAWER, SETTING};
enum valueMenuSetting {LIGHTING, CONTRAST, MUSIC, TIMING};
enum valueMusic {NONE, CASTELVANIA, DOOM, GODFATHER, MARIO, PACMAN, SONIC, STARWARS, ZELDA};

//Menu Var
char cursorMenuBase, cursorMenuSetting, cursorMenuMusic;
boolean displayExposure = false, exposureON = false, succesExpose = false;
boolean displaySetting = false;
boolean displaySetContrast = false, displaySetLight = false, displaySetMusic = false, displaySetTimer = false;

//Exposure
unsigned int totalExposure, totalTimeAbsolute, TimeIncr;
byte incrementExposure, defautIncrementExposure;

//LCD Contrast and Backlight
byte incrContrast, incrBacklight;


//Loading display
byte percent;

byte loading_0_4[8] = {B01111, B11000, B10000, B10000, B10000, B10000, B11000, B01111};
byte loading_1_4[8] = {B01111, B11000, B10000, B10000, B10000, B10011, B11000, B01111};
byte loading_2_4[8] = {B01111, B11000, B10000, B10000, B10111, B10011, B11000, B01111};
byte loading_3_4[8] = {B01111, B11000, B10000, B10111, B10111, B10011, B11000, B01111};
byte loading_4_4[8] = {B01111, B11000, B10011, B10111, B10111, B10011, B11000, B01111};

byte body_0_8[8] = {B11111, B00000, B00000, B00000, B00000, B00000, B00000, B11111};
byte body_1_8[8] = {B11111, B00000, B00000, B00000, B00000, B11000, B00000, B11111};
byte body_2_8[8] = {B11111, B00000, B00000, B00000, B11000, B11000, B00000, B11111};
byte body_3_8[8] = {B11111, B00000, B00000, B11000, B11000, B11000, B00000, B11111};
byte body_4_8[8] = {B11111, B00000, B11000, B11000, B11000, B11000, B00000, B11111};
byte body_5_8[8] = {B11111, B00000, B11000, B11000, B11000, B11011, B00000, B11111};
byte body_6_8[8] = {B11111, B00000, B11000, B11000, B11011, B11011, B00000, B11111};
byte body_7_8[8] = {B11111, B00000, B11000, B11011, B11011, B11011, B00000, B11111};
byte body_8_8[8] = {B11111, B00000, B11011, B11011, B11011, B11011, B00000, B11111};

byte end_0_4[8] = {B11110, B00011, B00001, B00001, B00001, B00001, B00011, B11110};
byte end_1_4[8] = {B11110, B00011, B00001, B00001, B00001, B11001, B00011, B11110};
byte end_2_4[8] = {B11110, B00011, B00001, B00001, B11101, B11001, B00011, B11110};
byte end_3_4[8] = {B11110, B00011, B00001, B11101, B11101, B11001, B00011, B11110};
byte end_4_4[8] = {B11110, B00011, B11001, B11101, B11101, B11001, B00011, B11110};


void initLCD() {
  pinMode(PIN_LCD_CONTRAST, OUTPUT);
  pinMode(PIN_LCD_BACKLIGHT, OUTPUT);

  lcd.begin(COLS, ROW);
  lcd.clear();

  cursorMenuBase = 0;

  //Init Fuse Backlight
  if (EEPROM.read (FUSE_LCD_BACKLIGHT) == 255) {
    EEPROM.update (FUSE_LCD_BACKLIGHT, 1); //Fuse dead
    EEPROM.update (SETLCD_BACKLIGHT, DEFAULT_LCD_BACKLIGHT); //Set default value timer increment
  }

  //Init Fuse Contrast LCD
  if (EEPROM.read (FUSE_LCD_CONTRAST) == 255) {
    EEPROM.update (FUSE_LCD_CONTRAST, 1); //Fuse dead
    EEPROM.update (SETLCD_CONTRAST, DEFAULT_LCD_CONTRAST); //Set default value timer increment
  }

  //Init FUSE Timer and set value
  if (EEPROM.read (FUSE_TIMER) == 255) {
    EEPROM.update (FUSE_TIMER, 1); //Fuse dead
    EEPROM.update (TIMER, DEFAUT_INCR_EXPOSURE); //Set default value timer increment
  }

  //Init FUSE Music and set value
  if (EEPROM.read (FUSE_MUSIC) == 255) {
    EEPROM.update (FUSE_MUSIC, 1); //Fuse dead
    EEPROM.update (SETMUSIC, PACMAN); //Set default value timer increment
  }

  incrementExposure = EEPROM.read (TIMER);
  cursorMenuMusic = EEPROM.read (SETMUSIC);

  incrContrast = EEPROM.read (SETLCD_CONTRAST);
  analogWrite(PIN_LCD_CONTRAST, incrContrast);

  incrBacklight = EEPROM.read (SETLCD_BACKLIGHT);
  analogWrite(PIN_LCD_BACKLIGHT, incrBacklight);

  totalExposure = 0;
  totalTimeAbsolute = 0;
  TimeIncr = 0;
  percent = 0;
}

//Initialise button state
void initButtonAndProgram() {
  pinMode(UP, INPUT_PULLUP);
  pinMode(DOWN, INPUT_PULLUP);
  pinMode(BACK, INPUT_PULLUP);
  pinMode(ENTER, INPUT_PULLUP);

  //Set Led level
  pinMode(LEDWAKING, OUTPUT);
  pinMode(LEDACTIVE, OUTPUT);
  
  digitalWrite(LEDWAKING, LED_HIGH);
  digitalWrite(LEDACTIVE, LED_LOW);

  //Set UV EXPOSURE
  pinMode(UVHOOD, OUTPUT);
  pinMode(UVDRAWER, OUTPUT);
    
  digitalWrite(UVHOOD, LOW);
  digitalWrite(UVDRAWER, LOW);

  //Buzzer
  pinMode(PIN_BUZZER, OUTPUT);
}

//Loading Bar init
void initLoading() {
  lcd.createChar(0, loading_4_4);
  lcd.createChar(1, body_0_8);
  lcd.createChar(2, body_8_8);
  lcd.createChar(3, end_0_4);
}


//Bank Sprite Loading Bar
void switchLoadingBank(byte bank) {
  switch (bank) {
    case 0:
      lcd.createChar(4, loading_0_4);
      lcd.createChar(5, loading_1_4);
      lcd.createChar(6, loading_2_4);
      lcd.createChar(7, loading_3_4);
      break;

    case 1:
      lcd.createChar(4, body_1_8);
      lcd.createChar(5, body_2_8);
      lcd.createChar(6, body_3_8);
      lcd.createChar(7, body_4_8);
      break;

    case 2:
      lcd.createChar(4, body_4_8);
      lcd.createChar(5, body_5_8);
      lcd.createChar(6, body_6_8);
      lcd.createChar(7, body_7_8);
      break;

    case 3:
      lcd.createChar(4, end_1_4);
      lcd.createChar(5, end_2_4);
      lcd.createChar(6, end_3_4);
      lcd.createChar(7, end_4_4);
      break;
  }
}


void lcdClear() {
  lcd.clear();
}


//Scroll text
void scrollRightToLeft (byte line, String str, char ending) {
  char cpt;

  for (cpt = 16; cpt > ending; cpt--) {
    lcd.setCursor(0, line);
    lcd.print(BLANK);
    delay(150);
    lcd.setCursor(cpt, line);
    lcd.print(str);

    delay(50);
  }
}


//Scroll text
void scrollLeftToRight (int line, String str, byte space) {
  byte cpt;
  char lenString;
  String tmpStr;

  for (cpt = 0; cpt < space; cpt++) {
    tmpStr += " ";
  }

  str = tmpStr + str;
  lenString = 40 - str.length();
  line -= 1;

  for (cpt = lenString; cpt <= lenString + 16; cpt++) {
    lcd.print(BLANK);
     delay(150);
    lcd.setCursor(cpt, line);
    lcd.print(str);

    delay(50);
  }
}


void twinkleDisplay(unsigned int timing, byte loop) {
  byte cpt;

  for (cpt = 0; cpt < loop; cpt++) {
    lcd.noDisplay();
    delay(timing);

    lcd.display();
    delay(timing);
  }

  lcd.clear();
}

/*
  void usageMem() {
  //View Usage Sram

  extern char *__data_start;
  extern char *__data_end;
  extern char *__bss_start;
  extern char *__bss_end;
  extern char *__heap_start;
  extern char *__heap_end;

  extern char *__brkval;

  uint16_t memReg = (uint16_t)&__data_start;
  uint16_t memData = (uint16_t)&__data_end - (uint16_t)&__data_start;
  uint16_t memBss = (uint16_t)&__bss_end - (uint16_t)&__bss_start;
  uint16_t memHeapTtl = ((uint16_t)__brkval == 0) ? 0 : (__brkval - __malloc_heap_start);

  uint16_t memStack = RAMEND - SP;
  uint16_t freeRam = SP - ((int)&__heap_start + memHeapTtl);

  Serial.println(F("==========RAM USAGE============="));
  Serial.println(String(memReg) + F(" => Registres, octets"));
  Serial.println(String(memData) + F(" => Data, octets"));
  Serial.println(String(memBss) + F(" => Bss, octets"));
  Serial.println(String(memHeapTtl) + F(" => Tas1, octets"));
  Serial.println(String(memStack) + F(" => Pile, octets"));
  Serial.println(String(freeRam) + F(" => FreeRam, octets"));
  Serial.println(F("==============================\n"));
  }
*/

//display number to lcd
void printIntValue(byte number, boolean timer) {
  char tmp[5];
  if (!timer) {
    tmp[0] = '0';
    tmp[1] = '0';
    tmp[2] = '0';
    tmp[3] = '%';
    tmp[4] = 0;
  }
  else {
    tmp[0] = '0';
    tmp[1] = '0';
    tmp[2] = '0';
    tmp[3] = 0;
  }

  if (number >= 100)
  {
    tmp[0]++;
    number -= 100;
  }
  if (number >= 100)
  {
    tmp[0]++;
    number -= 100;
  }

  if (number >= 50)
  {
    tmp[1] += 5;
    number -= 50;
  }
  if (number >= 10)
  {
    tmp[1]++;
    number -= 10;
  }
  if (number >= 10)
  {
    tmp[1]++;
    number -= 10;
  }
  if (number >= 10)
  {
    tmp[1]++;
    number -= 10;
  }
  if (number >= 10)
  {
    tmp[1]++;
    number -= 10;
  }

  tmp[2] += number;


  if (tmp[1] == '0' && tmp[0] == '0') {
    tmp[1] = ' ';
  }

  if (tmp[0] == '0') {
    tmp[0] = ' ';
  }

  lcd.print(tmp);
}


//Display loading bar
void displayLoadingBar(byte percent) {
  char tmp[5];
  byte nb_columns, cpt;

  lcd.setCursor(0, 1);
  nb_columns = map(percent, 0, 100, 0, (COLS - 4) * 2 * 4 - 2 * 4);

  //Generate sprite
  for (cpt = 0; cpt < COLS - 4; ++cpt) {
    if (cpt == 0) {
      if (nb_columns > 4) {
        lcd.write(byte(0));
        nb_columns -= 4;
      }
      else if (nb_columns == 4) {
        lcd.write(byte(0));
        nb_columns = 0;
      }
      else {
        switchLoadingBank(0);
        lcd.setCursor(cpt, 1);
        lcd.write(byte(nb_columns + 4));
        nb_columns = 0;
      }
    }
    else if (cpt == COLS - 5) {
      if (nb_columns > 0) {
        switchLoadingBank(3);
        lcd.setCursor(cpt, 1);
        lcd.write(byte(nb_columns + 3));
      }
      else {
        lcd.write(byte(3));
      }
    }
    else {
      if (nb_columns == 0) {
        lcd.write(byte(1));
      }
      else if (nb_columns >= 8) {
        lcd.write(byte(2));
        nb_columns -= 8;
      }
      else if (nb_columns >= 4 && nb_columns < 8) {
        switchLoadingBank(2);
        lcd.setCursor(cpt, 1);
        lcd.write(byte(nb_columns));
        nb_columns = 0;
      }
      else if (nb_columns < 4) {
        switchLoadingBank(1);
        lcd.setCursor(cpt, 1);
        lcd.write(byte(nb_columns + 3));
        nb_columns = 0;
      }
    }
  }

  printIntValue(percent, false); //add number percent
}


//Display lcd and actualise sprite loading bar and percent during expose UV, in Real Time
void calculExposeUV() {
  byte nb_columns;
  byte cpt;
  char tmp[5];

  digitalWrite(LEDWAKING, LED_LOW);
  digitalWrite(LEDACTIVE, LED_HIGH);
  delay(500);
  digitalWrite(LEDACTIVE, LED_LOW);
  delay(500);

  lcd.setCursor(0, 0);
  lcd.print(F("Min "));
  lcd.print(totalExposure / 60);
  lcd.write(32);
  lcd.setCursor(10, 0);
  lcd.print(F("Sec "));
  lcd.print(totalExposure % 60);
  lcd.write(32);

  initLoading();

  //Display Loading bar
  if (totalTimeAbsolute != totalExposure ) {
    TimeIncr++;
    percent = (TimeIncr * 100) / totalTimeAbsolute;
  }

  displayLoadingBar(percent);
}

//Display Exposure UV to lcd
void exposeUV() {
  lcd.home();

  if (cursorMenuBase == HOOD) {
    digitalWrite(UVHOOD, HIGH);
  }
  else if (cursorMenuBase == DRAWER) {
    digitalWrite(UVDRAWER, HIGH);
  }

  totalTimeAbsolute = totalExposure;

  for (totalExposure; totalExposure > 0 ; totalExposure--) {
    calculExposeUV();
  }

  calculExposeUV();  // for 99 to 100%
  delay(500);

  succesExpose = true;
  lcd.clear();
}

//Sprite success
void successGoodHand() {
  lcd.clear();
  lcd.home();
  byte hand1[8] = {B00100, B00011, B00100, B00011, B00100, B00011, B00010, B00001};
  byte hand2[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B00000, B00011};
  byte hand3[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B00001, B11110};
  byte hand4[8] = {B00000, B01100, B10010, B10010, B10001, B01000, B11110, B00000};
  byte hand5[8] = {B00010, B00010, B00010, B00010, B00010, B01110, B10000, B00000};
  byte hand6[8] = {B00000, B00000, B00000, B00000, B00000, B10000, B01000, B00110};
  lcd.createChar(0, hand1);
  lcd.createChar(1, hand2);
  lcd.createChar(2, hand3);
  lcd.createChar(3, hand4);
  lcd.createChar(4, hand5);
  lcd.createChar(5, hand6);
  lcd.setCursor(7, 1);
  lcd.write(byte(0));
  lcd.setCursor(7, 0);
  lcd.write(byte(1));
  lcd.setCursor(8, 1);
  lcd.write(byte(2));
  lcd.setCursor(8, 0);
  lcd.write(byte(3));
  lcd.setCursor(9, 1);
  lcd.write(byte(4));
  lcd.setCursor(9, 0);
  lcd.write(byte(5));
}


void successUV() {
  digitalWrite(UVHOOD, LOW);
  digitalWrite(UVDRAWER, LOW);
  digitalWrite(LEDWAKING, LED_HIGH);
  digitalWrite(LEDACTIVE, LED_LOW);
  
  //Re-init variable
  exposureON = false;
  percent = 0;
  totalExposure = 0;
  TimeIncr = 0;

  //View Hand good job
  successGoodHand();

  //Play Song
  if (EEPROM.read (SETMUSIC) != NONE) {
    musicSelect(PIN_BUZZER, EEPROM.read (SETMUSIC));
  }
}


//Button Interaction
void actionButton() {
  boolean buttonStateUP, buttonStateDOWN, buttonStateBACK, buttonStateENTER;

  buttonStateUP = digitalRead(UP);
  buttonStateDOWN = digitalRead(DOWN);
  buttonStateENTER = digitalRead(ENTER);

  //Button UP
  if (!buttonStateUP) {
    if (!exposureON && !succesExpose) {

      //Setting Menu
      if (displaySetting) {
        if (!displaySetContrast && !displaySetLight && !displaySetMusic && !displaySetTimer) {
          cursorMenuSetting++;
          if (cursorMenuSetting == 4) {
            cursorMenuSetting = 0;
          }

          do {
            delay(10);
          } while (digitalRead(UP) == BUTTON_HIGH);
        }

        //Config Timer
        if (displaySetTimer && incrementExposure < 255)  {
          incrementExposure ++;
          delay(60);
        }

        //Config Music
        if (displaySetMusic) {
          cursorMenuMusic ++;
          if (cursorMenuMusic == 9) {
            cursorMenuMusic = 0;
          }

          do {
            delay(10);
          } while (digitalRead(UP) == BUTTON_HIGH);
        }

        //Config Backlight
        if (displaySetLight) {
          if (incrBacklight < 255) {
            incrBacklight ++;
            analogWrite(PIN_LCD_BACKLIGHT, incrBacklight);
          }
        }

        //Config Contrast
        if (displaySetContrast) {
          if (incrContrast >= 1) {
            incrContrast --;
            analogWrite(PIN_LCD_CONTRAST, incrContrast);
          }
        }
      }
      else {
        //Base Menu
        if (!displayExposure) {
          cursorMenuBase++;
          if (cursorMenuBase == 3) {
            cursorMenuBase = 0;
          }

          do {
            delay(10);
          } while (digitalRead(UP) == BUTTON_HIGH);
        }
        else {
          totalExposure += incrementExposure;
          delay(300);
        }
      }

      lcd.setCursor(0, 0);
      lcd.print(BLANK);
    }
  }


  //Button Down
  if (!buttonStateDOWN) {
    if (!exposureON && !succesExpose) {
      //Setting Menu
      if (displaySetting) {
        if (!displaySetContrast && !displaySetLight && !displaySetMusic && !displaySetTimer) {
          cursorMenuSetting--;
          if (cursorMenuSetting == -1) {
            cursorMenuSetting = 3;
          }

          do {
            delay(10);
          } while (digitalRead(DOWN) == BUTTON_HIGH);
        }

        //Config Timer
        if (displaySetTimer && incrementExposure > 1)  {
          incrementExposure --;
          delay(60);
        }

        //Config Music
        if (displaySetMusic) {
          cursorMenuMusic --;

          if (cursorMenuMusic == -1) {
            cursorMenuMusic = 8;
          }

          do {
            delay(10);
          } while (digitalRead(DOWN) == BUTTON_HIGH);
        }

        //Config Backlight
        if (displaySetLight) {
          if (incrBacklight >= 1) {
            incrBacklight --;
            analogWrite(PIN_LCD_BACKLIGHT, incrBacklight);
          }
        }

        //Config contrast
        if (displaySetContrast) {
          if (incrContrast < 255) {
            incrContrast ++;
            analogWrite(PIN_LCD_CONTRAST, incrContrast);
          }
        }
      }
      else {
        //Base Menu
        if (!displayExposure) {
          cursorMenuBase--;
          if (cursorMenuBase == -1) {
            cursorMenuBase = 2;
          }

          do {
            delay(10);
          } while (digitalRead(DOWN) == BUTTON_HIGH);
        }
        else {
          if (totalExposure >= incrementExposure ) {
            totalExposure -= incrementExposure;
            delay(300);
          }
        }
      }

      lcd.setCursor(0, 0);
      lcd.print(BLANK);
    }
  }

  //Button Enter/Validate
  if (!buttonStateENTER) {
    if (!exposureON) {

      //Action Validate Setting
      if (displaySetTimer) { //Timer Save
        EEPROM.update (TIMER, incrementExposure);
        displaySetTimer = false;
        successGoodHand();
        delay(1000);
        lcd.clear();
      }
      else if (displaySetMusic) { //Set music
        EEPROM.update (SETMUSIC, cursorMenuMusic);
        displaySetMusic = false;
        successGoodHand();
        delay(1000);
        lcd.clear();
      }
      else {
        //Entrance Setting Menu
        if (displaySetting && !displaySetContrast && !displaySetLight && !displaySetMusic && !displaySetTimer) {
          switch (cursorMenuSetting) {
            case TIMING:
              displaySetTimer = true;
              lcd.clear();
              break;
            case MUSIC:
              displaySetMusic = true;
              lcd.clear();
              break;
            case LIGHTING:
              displaySetLight = true;
              break;
            case CONTRAST:
              displaySetContrast = true;
              break;
          }
        }
        else {
          if (!displayExposure && (cursorMenuBase == HOOD || cursorMenuBase == DRAWER) && !exposureON && !succesExpose) {
            //View Base Menu
            displayExposure = true;
          }
          else if (!displayExposure && cursorMenuBase == SETTING && !exposureON && !succesExpose) {
            //View Setting Menu
            displaySetting = true;
          }
          else if (displayExposure && !exposureON && totalExposure > 0 && !succesExpose) {
            //View Set Time Expose UV
            exposureON = true;
            initLoading();
          }
          else if (succesExpose) {
            //View Succes UV
            exposureON = false;
            succesExpose = false;
          }
        }
      }

      do {
        delay(10);
      } while (digitalRead(ENTER) == BUTTON_HIGH);

      lcd.clear();
      delay(5);
    }
  }
}


//Interruption Back Button gestion
void buttonBack() {
  if ( millis() - debounceTimeStamp >= 150  ) {
    //usageMem(); //Display to console line SRAM

    if (!exposureON) {
      if (displayExposure && (cursorMenuBase == HOOD || cursorMenuBase == DRAWER) && !exposureON && !succesExpose) {
        //View Base Menu
        displayExposure = false;
        totalExposure = 0;
        lcd.clear();
        delay(150);
        lcd.clear();
      }
      else if (!displayExposure && cursorMenuBase == SETTING && !exposureON && !succesExpose  && !displaySetContrast && !displaySetLight && !displaySetMusic && !displaySetTimer) {
        //View Setting Menu
        displaySetting = false;
        cursorMenuSetting = 0;
        lcd.clear();
        delay(150);
        lcd.clear();
      }

      //Action Setting Menu
      if (displaySetTimer) {
        displaySetTimer = false;
        incrementExposure = EEPROM.read (TIMER);
        lcd.clear();
        delay(150);
        lcd.clear();
      }

      //Setting Music Rollback
      if (displaySetMusic) {
        displaySetMusic = false;
        cursorMenuMusic = EEPROM.read (SETMUSIC);
        lcd.clear();
        delay(150);
        lcd.clear();
      }

      //Setting Backlight Save value
      if (displaySetLight) {
        displaySetLight = false;
        EEPROM.update (SETLCD_BACKLIGHT, incrBacklight);
        analogWrite(PIN_LCD_BACKLIGHT, incrBacklight);
        lcd.clear();
        delay(150);
        lcd.clear();
      }

      //Setting Contrast Save value
      if (displaySetContrast) {
        displaySetContrast = false;
        EEPROM.update (SETLCD_CONTRAST, incrContrast);
        analogWrite(PIN_LCD_CONTRAST, incrContrast);
        lcd.clear();
        delay(150);
        lcd.clear();
      }
    }
    else {
      totalExposure = 1;
      lcd.clear();
      delay(150);
      lcd.clear();
    }

    debounceTimeStamp = millis();
  }
}


//Set value to backlight screen
void setBacklight() {
  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print(F("BackLight "));

  lcd.setCursor(0, 1);
  lcd.print(F("<"));

  lcd.setCursor(5, 1);
  printIntValue(map(incrBacklight, 0, 255, 0, 100), false);

  lcd.setCursor(15, 1);
  lcd.print(F(">"));
}

//Set value to contrast letter for lcd
void setContrast() {
  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print(F("Contrast"));

  lcd.setCursor(0, 1);
  lcd.print(F("<"));

  lcd.setCursor(5, 1);
  printIntValue(map(incrContrast, 0, 255, 100, 0), false);

  lcd.setCursor(15, 1);
  lcd.print(F(">"));
}

//Set Increment value for timer to exposure UV
void setTimer() {
  lcd.setCursor(0, 0);
  lcd.print(F("Timer Incr"));
  lcd.setCursor(13, 0);
  printIntValue(incrementExposure, true);
  lcd.setCursor(5, 1);
  lcd.print(F("Set ?"));
}

//Set sound music to success finish exposure
void setMusic() {
  switch (cursorMenuMusic) {
    case NONE:
      lcd.setCursor(6, 0);
      lcd.print(F("NONE"));
      break;
    case CASTELVANIA:
      lcd.setCursor(2, 0);
      lcd.print(F("CASTELVANIA"));
      break;
    case DOOM:
      lcd.setCursor(6, 0);
      lcd.print(F("DOOM"));
      break;
    case GODFATHER:
      lcd.setCursor(3, 0);
      lcd.print(F("GOD FATHER"));
      break;
    case MARIO:
      lcd.setCursor(5, 0);
      lcd.print(F("MARIO"));
      break;
    case PACMAN:
      lcd.setCursor(5, 0);
      lcd.print(F("PACMAN"));
      break;
    case SONIC:
      lcd.setCursor(5, 0);
      lcd.print(F("SONIC"));
      break;
    case STARWARS:
      lcd.setCursor(4, 0);
      lcd.print(F("STARWARS"));
      break;
    case ZELDA:
      lcd.setCursor(5, 0);
      lcd.print(F("ZELDA"));
      break;
  }

  lcd.setCursor(0, 1);
  lcd.print("<");

  lcd.setCursor(5, 1);
  lcd.print(F("Set ?"));

  lcd.setCursor(15, 1);
  lcd.print(F(">"));
}


//Display Menu engine
void menu() {
  actionButton();

  //Setting menu
  if (displaySetting && !displaySetContrast && !displaySetLight && !displaySetMusic && !displaySetTimer) {
    //View Setting Menu
    switch (cursorMenuSetting) {
      case CONTRAST:
        lcd.setCursor(4, 0);
        lcd.print(F("Contrast"));
        break;
      case LIGHTING:
        lcd.setCursor(3, 0);
        lcd.print(F("BackLight"));
        break;
      case MUSIC:
        lcd.setCursor(3, 0);
        lcd.print(F("Set Music"));
        break;
      case TIMING:
        lcd.setCursor(1, 0);
        lcd.print(F("Set Incr Timer"));
        break;
    }

    lcd.setCursor(0, 1);
    lcd.print(F("<"));

    lcd.setCursor(5, 1);
    lcd.print(F("Enter"));

    lcd.setCursor(15, 1);
    lcd.print(F(">"));
  }
  else {
    //Base menu
    if (!displayExposure && !exposureON && !displaySetContrast && !displaySetLight && !displaySetMusic && !displaySetTimer) {
      //View Base Menu
      switch (cursorMenuBase) {
        case HOOD:
          lcd.setCursor(3, 0);
          lcd.print(F("Expose Hood"));
          break;
        case DRAWER:
          lcd.setCursor(1, 0);
          lcd.print(F("Expose Drawer"));
          break;
        case SETTING:
          lcd.setCursor(4, 0);
          lcd.print(F("Setting"));
          break;
      }

      lcd.setCursor(0, 1);
      lcd.print(F("<"));

      lcd.setCursor(5, 1);
      lcd.print(F("Enter"));

      lcd.setCursor(15, 1);
      lcd.print(F(">"));
    }

    //Set Backlight
    if (displaySetLight) {
      setBacklight();
    }


    //Set Contrast
    if (displaySetContrast) {
      setContrast();
    }

    //Set Time Incr
    if (displaySetTimer) {
      setTimer();
    }

    //Set Music
    if (displaySetMusic) {
      setMusic();
    }


    //Exposure Set Time
    if (displayExposure && !exposureON && !succesExpose) {
      lcd.setCursor(4, 0);
      lcd.print(F("Set Time"));

      lcd.setCursor(0, 1);
      lcd.print(F("Min "));
      lcd.print(totalExposure / 60);
      lcd.write(32);
      lcd.setCursor(10, 1);
      lcd.print(F("Sec "));
      lcd.print(totalExposure % 60);
      lcd.write(32);
    }

    //Start Expose UV
    if (displayExposure && exposureON && !succesExpose && totalExposure > 0) {
      exposeUV();
    }

    //Success exposeUV
    if (succesExpose && exposureON) {
      successUV();
    }
  }
}
