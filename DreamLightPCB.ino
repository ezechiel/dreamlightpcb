/**
 * Exposure UV DREAM LIGHT PCB
 * 
 * Descr : Arduino program for expose PCB backerlite or epoxy with UV LED. Ideal for create new PCB or serigraphy. User choose timing UV, hood or drawer  
 * Author : Ezechiel
 * Date : 2019-11-11
 * 
 * Compatible Arduino Nano V3
 */

#include "Functions.h"

void setup() {
  attachInterrupt(0, buttonBack, FALLING);
  initButtonAndProgram();
  
  //Set LCD 16x2
  initLCD();
  initLoading();
  scrollRightToLeft(0, F("DreamLightPCB"), 1);
  scrollLeftToRight(1, F("By Ezechiel"), 3);
  delay(500);
  
  
  
  //Go Menu
  twinkleDisplay(400, 2);
  lcdClear();
  delay(200);
}

void loop() {
   menu();
   delay(50);
}
