#include "Arduino.h"

#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978
#define REST      0

enum valueMusic {NONE, CASTELVANIA, DOOM, GODFATHER, MARIO, PACMAN, SONIC, STARWARS, ZELDA};

const int melodyDoom[] PROGMEM = {
  NOTE_E2, 8, NOTE_E2, 8, NOTE_E3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_D3, 8, NOTE_E2, 8, NOTE_E2, 8, //1
  NOTE_C3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_AS2, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_B2, 8, NOTE_C3, 8,
  NOTE_E2, 8, NOTE_E2, 8, NOTE_E3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_D3, 8, NOTE_E2, 8, NOTE_E2, 8,
  NOTE_C3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_AS2, -2,

  NOTE_E2, 8, NOTE_E2, 8, NOTE_E3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_D3, 8, NOTE_E2, 8, NOTE_E2, 8, //5
  NOTE_C3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_AS2, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_B2, 8, NOTE_C3, 8,
  NOTE_E2, 8, NOTE_E2, 8, NOTE_E3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_D3, 8, NOTE_E2, 8, NOTE_E2, 8,
  NOTE_C3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_AS2, -2,

  NOTE_E2, 8, NOTE_E2, 8, NOTE_E3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_D3, 8, NOTE_E2, 8, NOTE_E2, 8, //13
  NOTE_C3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_AS2, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_B2, 8, NOTE_C3, 8,
  NOTE_E2, 8, NOTE_E2, 8, NOTE_E3, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_D3, 8, NOTE_E2, 8, NOTE_E2, 8,
  NOTE_FS3, -16, NOTE_D3, -16, NOTE_B2, -16, NOTE_A3, -16, NOTE_FS3, -16, NOTE_B2, -16, NOTE_D3, -16, NOTE_FS3, -16, NOTE_A3, -16, NOTE_FS3, -16, NOTE_D3, -16, NOTE_B2, -16,
};


const int melodyPacman[] PROGMEM = {
  NOTE_B4, 16, NOTE_B5, 16, NOTE_FS5, 16, NOTE_DS5, 16, //1
  NOTE_B5, 32, NOTE_FS5, -16, NOTE_DS5, 8, NOTE_C5, 16,
  NOTE_C6, 16, NOTE_G6, 16, NOTE_E6, 16, NOTE_C6, 32, NOTE_G6, -16, NOTE_E6, 8,

  NOTE_B4, 16,  NOTE_B5, 16,  NOTE_FS5, 16,   NOTE_DS5, 16,  NOTE_B5, 32,  //2
  NOTE_FS5, -16, NOTE_DS5, 8,  NOTE_DS5, 32, NOTE_E5, 32,  NOTE_F5, 32,
  NOTE_F5, 32,  NOTE_FS5, 32,  NOTE_G5, 32,  NOTE_G5, 32, NOTE_GS5, 32,  NOTE_A5, 16, NOTE_B5, 8
};


const int melodySonic[] PROGMEM = {
  REST, 2, NOTE_D5, 8, NOTE_B4, 4, NOTE_D5, 8, //1
  NOTE_CS5, 4, NOTE_D5, 8, NOTE_CS5, 4, NOTE_A4, 2,
  REST, 8, NOTE_A4, 8, NOTE_FS5, 8, NOTE_E5, 4, NOTE_D5, 8,
  NOTE_CS5, 4, NOTE_D5, 8, NOTE_CS5, 4, NOTE_A4, 2,
  REST, 4, NOTE_D5, 8, NOTE_B4, 4, NOTE_D5, 8,
  NOTE_CS5, 4, NOTE_D5, 8, NOTE_CS5, 4, NOTE_A4, 2,

  REST, 8, NOTE_B4, 8, NOTE_B4, 8, NOTE_G4, 4, NOTE_B4, 8, //7
  NOTE_A4, 4, NOTE_B4, 8, NOTE_A4, 4, NOTE_D4, 2,
  REST, 4, NOTE_D5, 8, NOTE_B4, 4, NOTE_D5, 8,
  NOTE_CS5, 4, NOTE_D5, 8, NOTE_CS5, 4, NOTE_A4, 2,
  REST, 8, NOTE_A4, 8, NOTE_FS5, 8, NOTE_E5, 4, NOTE_D5, 8,
  NOTE_CS5, 4, NOTE_D5, 8, NOTE_CS5, 4, NOTE_A4, 2,

  REST, 4, NOTE_D5, 8, NOTE_B4, 4, NOTE_D5, 8, //13
  NOTE_CS5, 4, NOTE_D5, 8, NOTE_CS5, 4, NOTE_A4, 2,
  REST, 8, NOTE_B4, 8, NOTE_B4, 8, NOTE_G4, 4, NOTE_B4, 8,
  NOTE_A4, 4, NOTE_B4, 8, NOTE_A4, 4, NOTE_D4, 8, NOTE_D4, 8, NOTE_FS4, 8,
  NOTE_E4, -1,
  REST, 8, NOTE_D4, 8, NOTE_E4, 8, NOTE_FS4, -1,

  REST, 8, NOTE_D4, 8, NOTE_D4, 8, NOTE_FS4, 8, NOTE_F4, -1, //20
  REST, 8, NOTE_D4, 8, NOTE_F4, 8, NOTE_E4, -1, //end 1
};


const int melodyImperial[] PROGMEM = {
  // Score available at https://musescore.com/user/202909/scores/1141521
  NOTE_A4, -4, NOTE_A4, -4, NOTE_A4, 16, NOTE_A4, 16, NOTE_A4, 16, NOTE_A4, 16, NOTE_F4, 8, REST, 8,
  NOTE_A4, -4, NOTE_A4, -4, NOTE_A4, 16, NOTE_A4, 16, NOTE_A4, 16, NOTE_A4, 16, NOTE_F4, 8, REST, 8,
  NOTE_A4, 4, NOTE_A4, 4, NOTE_A4, 4, NOTE_F4, -8, NOTE_C5, 16,

  NOTE_A4, 4, NOTE_F4, -8, NOTE_C5, 16, NOTE_A4, 2, //4
  NOTE_E5, 4, NOTE_E5, 4, NOTE_E5, 4, NOTE_F5, -8, NOTE_C5, 16,
  NOTE_A4, 4, NOTE_F4, -8, NOTE_C5, 16, NOTE_A4, 2,

  NOTE_A5, 4, NOTE_A4, -8, NOTE_A4, 16, NOTE_A5, 4, NOTE_GS5, -8, NOTE_G5, 16, //7
  NOTE_DS5, 16, NOTE_D5, 16, NOTE_DS5, 8, REST, 8, NOTE_A4, 8, NOTE_DS5, 4, NOTE_D5, -8, NOTE_CS5, 16,

  NOTE_C5, 16, NOTE_B4, 16, NOTE_C5, 16, REST, 8, NOTE_F4, 8, NOTE_GS4, 4, NOTE_F4, -8, NOTE_A4, -16, //9
  NOTE_C5, 4, NOTE_A4, -8, NOTE_C5, 16, NOTE_E5, 2,

  NOTE_A5, 4, NOTE_A4, -8, NOTE_A4, 16, NOTE_A5, 4, NOTE_GS5, -8, NOTE_G5, 16, //7
  NOTE_DS5, 16, NOTE_D5, 16, NOTE_DS5, 8, REST, 8, NOTE_A4, 8, NOTE_DS5, 4, NOTE_D5, -8, NOTE_CS5, 16,

  NOTE_C5, 16, NOTE_B4, 16, NOTE_C5, 16, REST, 8, NOTE_F4, 8, NOTE_GS4, 4, NOTE_F4, -8, NOTE_A4, -16, //9
  NOTE_A4, 4, NOTE_F4, -8, NOTE_C5, 16, NOTE_A4, 2,
};


const int melodyMario[] PROGMEM = {
  NOTE_E5, 8, NOTE_E5, 8, REST, 8, NOTE_E5, 8, REST, 8, NOTE_C5, 8, NOTE_E5, 8, //1
  NOTE_G5, 4, REST, 4, NOTE_G4, 8, REST, 4,
  NOTE_C5, -4, NOTE_G4, 8, REST, 4, NOTE_E4, -4, // 3
  NOTE_A4, 4, NOTE_B4, 4, NOTE_AS4, 8, NOTE_A4, 4,
  NOTE_G4, -8, NOTE_E5, -8, NOTE_G5, -8, NOTE_A5, 4, NOTE_F5, 8, NOTE_G5, 8,
  REST, 8, NOTE_E5, 4, NOTE_C5, 8, NOTE_D5, 8, NOTE_B4, -4,
  NOTE_C5, -4, NOTE_G4, 8, REST, 4, NOTE_E4, -4, // repeats from 3
  NOTE_A4, 4, NOTE_B4, 4, NOTE_AS4, 8, NOTE_A4, 4,
  NOTE_G4, -8, NOTE_E5, -8, NOTE_G5, -8, NOTE_A5, 4, NOTE_F5, 8, NOTE_G5, 8,
  REST, 8, NOTE_E5, 4, NOTE_C5, 8, NOTE_D5, 8, NOTE_B4, -4,


  REST, 4, NOTE_G5, 8, NOTE_FS5, 8, NOTE_F5, 8, NOTE_DS5, 4, NOTE_E5, 8, //7
  REST, 8, NOTE_GS4, 8, NOTE_A4, 8, NOTE_C4, 8, REST, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_D5, 8,
  REST, 4, NOTE_DS5, 4, REST, 8, NOTE_D5, -4,
  NOTE_C5, 2, REST, 2,

  REST, 4, NOTE_G5, 8, NOTE_FS5, 8, NOTE_F5, 8, NOTE_DS5, 4, NOTE_E5, 8, //repeats from 7
  REST, 8, NOTE_GS4, 8, NOTE_A4, 8, NOTE_C4, 8, REST, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_D5, 8,
  REST, 4, NOTE_DS5, 4, REST, 8, NOTE_D5, -4,
  NOTE_C5, 2, REST, 2,
};


const int melodyParrain[] PROGMEM = {
  REST, 4, REST, 8, REST, 8, REST, 8, NOTE_E4, 8, NOTE_A4, 8, NOTE_C5, 8, //1
  NOTE_B4, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_A4, 8, NOTE_B4, 8, NOTE_A4, 8, NOTE_F4, 8, NOTE_G4, 8,
  NOTE_E4, 2, NOTE_E4, 8, NOTE_A4, 8, NOTE_C5, 8,
  NOTE_B4, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_A4, 8, NOTE_E4, 8, NOTE_DS4, 8,

  NOTE_D4, 2, NOTE_D4, 8, NOTE_F4, 8, NOTE_GS4, 8, //5
  NOTE_B4, 2, NOTE_D4, 8, NOTE_F4, 8, NOTE_GS4, 8,
  NOTE_A4, 2, NOTE_C4, 8, NOTE_C4, 8, NOTE_G4, 8,
  NOTE_F4, 8, NOTE_E4, 8, NOTE_G4, 8, NOTE_F4, 8, NOTE_F4, 8, NOTE_E4, 8, NOTE_E4, 8, NOTE_GS4, 8,

  NOTE_A4, 2, REST, 8, NOTE_A4, 8, NOTE_A4, 8, NOTE_GS4, 8, //9
  NOTE_G4, 2, NOTE_B4, 8, NOTE_A4, 8, NOTE_F4, 8,
  NOTE_E4, 2, NOTE_E4, 8, NOTE_G4, 8, NOTE_E4, 8,
  NOTE_D4, 2, NOTE_D4, 8, NOTE_D4, 8, NOTE_F4, 8, NOTE_DS4, 8
};


const int melodyCasterlvania[] PROGMEM = {
  NOTE_E5, 16, NOTE_E5, 8, NOTE_D5, 16, REST, 16, NOTE_CS5, -4, NOTE_E4, 8, NOTE_FS4, 16, NOTE_G4, 16, NOTE_A4, 16,

  NOTE_B4, -8, NOTE_E4, -8, NOTE_B4, 8, NOTE_A4, 16, NOTE_D5, -4,
  NOTE_E5, 16, NOTE_E5, 8, NOTE_D5, 16, REST, 16, NOTE_CS5, -4, NOTE_E4, 8, NOTE_FS4, 16, NOTE_G4, 16, NOTE_A4, 16,
  NOTE_B4, -8, NOTE_E4, -8, NOTE_B4, 8, NOTE_A4, 16, NOTE_D4, -4,

  NOTE_DS4, -8, NOTE_FS4, -8, NOTE_C5, 8, NOTE_B4, -8, NOTE_G4, -8, NOTE_E4, 8, //14
  NOTE_DS4, -8, NOTE_FS4, -8, NOTE_C5, 8, NOTE_B4, -8, NOTE_G4, -8, REST, 8,
  NOTE_DS4, -8, NOTE_FS4, -8, NOTE_C5, 8, NOTE_B4, -8, NOTE_G4, -8, NOTE_E4, 8,
  NOTE_DS4, -8, NOTE_FS4, -8, NOTE_C5, 8, NOTE_B4, -8, NOTE_CS5, -8, NOTE_DS5, 8,

  NOTE_E5, 16, NOTE_E5, 16, NOTE_E4, 16, NOTE_E4, -2, //18
  NOTE_C4, 8, NOTE_C4, 8, NOTE_E4, 16, NOTE_G4, -8, NOTE_D4, 8, NOTE_D4, 8, NOTE_FS4, 16, NOTE_A4, -8,
  NOTE_E5, 16, NOTE_E5, 16, NOTE_E4, 16, NOTE_E4, -2,
  NOTE_C4, 8, NOTE_C4, 8, NOTE_E4, 16, NOTE_G4, -8, NOTE_D4, 8, NOTE_D4, 8, NOTE_B3, 16, NOTE_D4, -8,

  NOTE_B4, -8, NOTE_E4, -8, NOTE_B4, 8, NOTE_A4, 16, NOTE_D5, -4,
  NOTE_E5, 16, NOTE_E5, 8, NOTE_D5, 16, REST, 16, NOTE_CS5, -4, NOTE_E4, 8, NOTE_FS4, 16, NOTE_G4, 16, NOTE_A4, 16,
  NOTE_B4, -8, NOTE_E4, -8, NOTE_B4, 8, NOTE_A4, 16, NOTE_D4, -4,
};


const int melodyZelda[] PROGMEM = {
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, 2, NOTE_C4, 8, NOTE_D4, 8,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, -2,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D5, 2, NOTE_C5, 4,
  NOTE_G4, 2, NOTE_F4, 8, NOTE_E4, 8,
  NOTE_D4, -2,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, 2, NOTE_C4, 8, NOTE_D4, 8,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, -2,
  NOTE_E4, 2, NOTE_G4, 4,

  NOTE_D5, 2, NOTE_C5, 4,
  NOTE_G4, 2, NOTE_F4, 8, NOTE_E4, 8,
  NOTE_F4, 8, NOTE_E4, 8, NOTE_C4, 2,
  NOTE_F4, 2, NOTE_E4, 8, NOTE_D4, 8,
  NOTE_E4, 8, NOTE_D4, 8, NOTE_A3, 2,
  NOTE_G4, 2, NOTE_F4, 8, NOTE_E4, 8,
  NOTE_F4, 8, NOTE_E4, 8, NOTE_C4, 4, NOTE_F4, 4,
  NOTE_C5, -2,
};


void playSong(byte buzzer, byte tempo, byte notes, const int song[] PROGMEM) {
  byte thisNote;
  unsigned int wholenote, noteDuration;
  int divider;

  divider = 0;
  noteDuration = 0;
  wholenote = (60000 * 4) / tempo;

  for (thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {
    divider = pgm_read_word_near(song + thisNote + 1);

    if (divider > 0) {
      noteDuration = (wholenote) / divider;
    }
    else if (divider < 0) {
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5;
    }

    tone(buzzer, pgm_read_word_near(song + thisNote), noteDuration * 0.9);
    delay(noteDuration);
    noTone(buzzer);
  }
}


void musicSelect(byte buzzer, byte music) {
  switch (music) {
    case NONE:
      break;
    case CASTELVANIA:
      playSong(buzzer, 130, (byte) (sizeof(melodyCasterlvania) / sizeof(melodyCasterlvania[0]) / 2), melodyCasterlvania);
      break;
    case DOOM:
      playSong(buzzer, 225, (byte) (sizeof(melodyDoom) / sizeof(melodyDoom[0]) / 2), melodyDoom);
      break;
    case GODFATHER:
      playSong(buzzer, 80, (byte) (sizeof(melodyParrain) / sizeof(melodyParrain[0]) / 2), melodyParrain);
      break;
    case MARIO:
      playSong(buzzer, 200, (byte) (sizeof(melodyMario) / sizeof(melodyMario[0]) / 2), melodyMario);
      break;
    case PACMAN:
      playSong(buzzer, 105, (byte) (sizeof(melodyPacman) / sizeof(melodyPacman[0]) / 2), melodyPacman);
      break;
    case SONIC:
      playSong(buzzer, 140, (byte) (sizeof(melodySonic) / sizeof(melodySonic[0]) / 2), melodySonic);
      break;
    case STARWARS:
      playSong(buzzer, 120, (byte) (sizeof(melodyImperial) / sizeof(melodyImperial[0]) / 2), melodyImperial);
      break;
    case ZELDA:
      playSong(buzzer, 108, (byte) (sizeof(melodyZelda) / sizeof(melodyZelda[0]) / 2), melodyZelda);
      break;
  }
}
